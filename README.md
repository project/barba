## INTRODUCTION

The Barba module provides asynchronous page loading with barba.js.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Enable the Barba module.
- Add the `{{ barba_container }}` to an element of your theme's `page.html.twig` template (i.e. <main {{ barba_container }}>)

## MAINTAINERS

Current maintainers for Drupal 10:

- Adam Blankenship (adam_bear) - https://www.drupal.org/u/adam_bear

